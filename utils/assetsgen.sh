#! /bin/sh

cd assets
rm -f *.h
for filename in *; do
    xxd -i "$filename" > "$filename".h
done
