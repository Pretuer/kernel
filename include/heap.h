#include <stdbool.h>


struct memchunk_free {
    uint64_t size;
    struct memchunk_free *next;
};


void heap_init();
void *malloc(uint64_t size);
void *heap_allocate(uint64_t *size);
void free(void *memptr);
void heap_deallocate(void *memptr, uint64_t size);
