#pragma once
#include "stivale.h"

struct coords {
    uint16_t x, y;
};
struct pixel24 {
    unsigned char B, G, R;
};
struct pixel32 {
    unsigned char B, G, R, A;
};


#define FRAMEBUFFER ((struct pixel32 *)stivale->framebuffer_addr)
#define FB_LOCATE(x, y) (stivale->framebuffer_width * (y) + (x))
