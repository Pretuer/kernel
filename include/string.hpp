namespace std {
class string {
    unsigned long reallen = 0;
    unsigned long buflen = 10;

    void fit();

public:
    char *buf;

    inline auto size() const {
        return reallen;
    }
    inline auto length() const {
        return size();
    }
    inline char operator [](unsigned long index) const {
        return buf[index];
    }
    inline char back() const {
        return buf[reallen - 1];
    }

    string();
    string(const string& src);
    string(const char *src);
    string(const char *src, unsigned long len);
    void push_back(char character);
    void append(const string& src);
    string operator +(const string& src);
    const char *c_str();
};
}
