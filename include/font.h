#include <stdbool.h>
#define FONT_HEIGHT 16
#define FONT_WIDTH  8
#define FONT_SIZE   FONT_HEIGHT * FONT_WIDTH

extern const bool fontBitmapUnk[FONT_SIZE];

const bool *font_get_character(const char character);
