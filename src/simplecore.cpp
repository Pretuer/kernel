#include "string.hpp"
extern "C" {
#include "noreturn.h"
#include "stivale.h"
#include "panic.h"
#include "textio.h"
#include "stdio.h"
#include "interrupts.h"
#include "keyboard.h"
#include "heap.h"
#include "string.h"
}


[[noreturn]]
void kmainpp() {
    /*
    print("struct stivale_struct stivale = {\n"); {
        #define STIVALE_DEBUG(key, type) printf("    ."#key" = %"#type",\n", stivale->key);
        STIVALE_DEBUG(cmdline, x);
        STIVALE_DEBUG(memory_map_addr, x);
        STIVALE_DEBUG(memory_map_entries, l);
        STIVALE_DEBUG(framebuffer_addr, x);
        STIVALE_DEBUG(framebuffer_pitch, l);
        STIVALE_DEBUG(framebuffer_width, l);
        STIVALE_DEBUG(framebuffer_height, l);
        STIVALE_DEBUG(framebuffer_bpp, l);
        STIVALE_DEBUG(rsdp, x);
        STIVALE_DEBUG(module_count, l);
        STIVALE_DEBUG(modules, x);
        STIVALE_DEBUG(epoch, l);
    } print("};\n");
    */

    {
        std::string test = "Simple ";
        test.append("string test\n");
        print(test.c_str());

        print("Simple draw-anywhere test");
        auto cursor_bak = textio_cursor;
        textio_cursor.y += 5;
        print("Hello world!");
        textio_cursor = cursor_bak;
        print("\n\n");

        auto font_color_fg_bak = font_color_fg;
        auto font_color_bg_bak = font_color_bg;
        font_color_fg = {
            255,
            0,
            0
        };
        font_color_bg = {
            0,
            0,
            255
        };
        print("Font color test\n");
        font_color_fg = font_color_fg_bak;
        font_color_bg = font_color_bg_bak;

        //
    }

    while(1) {
        char inchar;
        while (!(inchar = kb_getchar()));
        putchar(inchar);
    }
}

extern "C" {
[[noreturn]]
void kmain(struct stivale_struct* strct) {
    // Set doublefault handler
    register_interrupt_handler(0x08, reinterpret_cast<void*>(cpu_exception), 0, INTERRUPT_GATE);
    // Initiliase environemt
    init_from_stivale(strct);
    heap_init();
    textio_init();
    kb_init();
    idt_init();
    kb_enable();
    // Switch to C++ code
    kmainpp();
}
}
