#include "string.hpp"
extern "C" {
#include "string.h"
}
constexpr size_t rahead = 10;


namespace std {

string::string() {
    buf = new char[buflen];
}
string::string(const char *src) {
    reallen = strlen(src);
    buflen = reallen + rahead;
    buf = new char[buflen];
    strcpy(buf, src);
}
string::string(const char *src, unsigned long len) {
    reallen = len;
    buflen = reallen + rahead;
    buf = new char[buflen];
    memcpy(buf, src, len);
}
string::string(const string& src) {
    reallen = src.size();
    buflen = reallen + rahead;
    buf = new char[buflen];
    memcpy(buf, src.buf, src.size());
}

void string::fit() {
    if (reallen > buflen) {
        buflen = reallen + rahead;
        auto newbuf = new char[buflen];
        memcpy(newbuf, buf, reallen);
        delete buf;
        buf = newbuf;
    }
}

void string::push_back(char character) {
    reallen++;
    fit();
    buf[reallen-1] = character;
}

void string::append(const string& src) {
    reallen += src.size();
    fit();
    memcpy(buf + reallen - src.size(), src.buf, src.size());
}

string string::operator +(const string& src) {
    string cpy = *this;
    cpy.append(src);
    return cpy;
}

const char *string::c_str() {
    if (back() != '\0') {
        push_back('\0');
    }
    reallen--;
    return buf;
}

}
