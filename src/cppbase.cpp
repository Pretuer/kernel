#include <cstddef>
#include <cstdint>
extern "C" {
#include "heap.h"
}

void *operator new(size_t len) {
    return malloc(len);
}
void *operator new[](size_t len) {
    return malloc(len);
}
void operator delete(void *ptr, unsigned long) {
    free(ptr);
}
