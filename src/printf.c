#include <stdarg.h>
#include <stdbool.h>
#include <limits.h>
#include "string.h"
#include "textio.h"


void print_hex_impl(uint64_t num, int nibbles, bool padding) {
    // Check if number is 0
    if (num == 0 && !padding) {
        putchar('0');
        return;
    }
    // Start printing
    bool ispadding = !padding;
    for(int i = nibbles - 1; i >= 0; -- i) {
        char out = "0123456789ABCDEF"[(num >> (i * 4))&0xF];
        // If number isn't 0, this isn't padding
        if (out != '0') {
            ispadding = false;
        }
        // If no more padding, print
        if (!ispadding) {
            putchar(out);
        }
    }
}

void print_dec(uint64_t num) {
    // Output string
    char numstr[21]; // 20 is maximum amount of digits in num
    numstr[sizeof(numstr) - 1] = '\0'; // Has to be null-terminated
    char *numstart = numstr + sizeof(numstr) - 2;
    // Check if number is signed
    if (num == 0) {
        putchar('0');
        return;
    }
    // Convert to string
    while (num != 0) {
      *(numstart--) = num % 10 + 0x30;
      num = num / 10;
    }
    // Print result
    print(numstart + 1);
}

int printf(const char *format, ...) {
    va_list ap;
    bool nexteval = false;
    va_start(ap, format);
    for (const char *thischar = format; *thischar != '\0'; thischar++) {
        if (*thischar == '%') {
            if (!(nexteval = !nexteval)) {
                putchar('%');
            }
        } else if (nexteval) {
            switch(*thischar) {
                case 's': print(va_arg(ap, const char *)); break;
                case 'c': putchar((const char)va_arg(ap, const int)); break;
                case 'x': print("0x"); case 'p': print_hex(va_arg(ap, uint64_t), true); break;
                case 'd': case 'i': case 'l': print_dec(va_arg(ap, uint64_t)); break;
                default: print("<unknown type>");
            }
            nexteval = false;
        } else {
            putchar(*thischar);
        }
    }
    va_end(ap);
}
