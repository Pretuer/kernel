#! /bin/bash


# Config
COMPILER="gcc"
COMPILER_FLAGS="-m64 -c -fno-stack-protector -fno-pic -mno-sse -mno-sse2 -mno-mmx -mno-red-zone -mno-80387 -mcmodel=kernel -ffreestanding -fno-stack-protector -fno-omit-frame-pointer -msoft-float"
COMPILER_INCLUEDIRS="-Iassets -Iinclude"
ASSEMBLER="nasm"
ASSEMBLER_FLAGS=""
ASSEMBLER_TARGET="elf64"
LINKER="ld"
LINKER_FLAGS="-T link.ld"
LINKER_TARGET="elf_x86_64"


# Library functions
function file_break {
    test -f "$1" || echo break
}
function getoutfile {
    echo obj/"$(basename "${1%.*}")-"${1##*.}"".o
}
function makeentry_init {
    srcfile="$1"
    outfile="$(getoutfile $srcfile)"
    outfiles+=("$outfile")
    echo " - ${srcfile} produces ${outfile}..." > /dev/stderr
}

# Templates
function makeentry_cfile {
    makeentry_init "$1"
    echo "
${outfile}: ${srcfile}
	${COMPILER} ${COMPILER_FLAGS} ${COMPILER_INCLUEDIRS} -c ${srcfile} -o ${outfile}"
}
function makeentry_cppfile {
    makeentry_init "$1"
    echo "
${outfile}: ${srcfile}
	${COMPILER} -std=c++17 ${COMPILER_FLAGS} ${COMPILER_INCLUEDIRS} -c ${srcfile} -o ${outfile}"
}
function makeentry_sfile {
    makeentry_init "$1"
    echo "
${outfile}: ${srcfile}
	${ASSEMBLER} ${ASSEMBLER_FLAGS} -f ${ASSEMBLER_TARGET} ${srcfile} -o ${outfile}"
}
function makeentry_misc {
    echo "all: link"
    echo "compile: ${outfiles[@]}"
    echo "link: compile
	${LINKER} -o bin/$proj_name -m ${LINKER_TARGET} ${LINKER_FLAGS} ${outfiles[@]}"
    echo "clean:
	rm -f ${outfiles[@]} bin/$proj_name"
    echo ""
}

# Create variables
declare -a outfiles
proj_name="simplecore"

# Prepare structure
mkdir -p obj bin

# Generate Makefile
for filename in src/*.c; do
    $(file_break "$filename")
    makeentry_cfile "$filename" >> Makefile_body
done
for filename in src/*.cpp; do
    $(file_break "$filename")
    makeentry_cppfile "$filename" >> Makefile_body
done
for filename in src/*.s; do
    $(file_break "$filename")
    makeentry_sfile "$filename" >> Makefile_body
done
makeentry_misc > Makefile
cat Makefile_body >> Makefile
if [ -f "Makefile_cst" ] ; then
    echo >> Makefile
    cat Makefile_cst >> Makefile
fi
rm -f Makefile_body
